﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheVoid
{
    public static class Options
    {
        private static bool showError = false;
        /// <summary>
        /// A property to show or hide error messages 
        /// (default = false)
        /// </summary>
        public static bool ShowError
        {
            get { return showError; }
            set { showError = value; }
        }

        /* **************************************************************************
         * **************************************************************************/

        /// <summary>
        /// To read a registry key.
        /// input: KeyName (string)
        /// output: value (string) 
        /// </summary>
        public static T Read<T>(string KeyName, T ifnullvalue)
        {
            return ifnullvalue;


        }

        /// <summary>
        /// To read a registry key.
        /// input: KeyName (string)
        /// output: value (string) 
        /// </summary>
        public static int Read(string KeyName, int ifnullvalue = -1)
        {
            int result = Convert.ToInt16(Read(KeyName, ifnullvalue.ToString()));
            return result == -1 ? ifnullvalue : result;
        }


        /* **************************************************************************
         * **************************************************************************/

        /// <summary>
        /// To write into a registry key.
        /// input: KeyName (string) , Value (object)
        /// output: true or false 
        /// </summary>
        public static bool Write<T>(string KeyName, T Value)
        {
            Console.WriteLine($"Options Write Attepted!!! [{KeyName}]{Value}");
            return false;
        }



        /* **************************************************************************
         * **************************************************************************/

        private static void ShowErrorMessage(Exception e, string Title)
        {
            // if (showError == true)
            Utility.Print(e.Message);
        }
    }

}
