﻿using System;
using System.Net;
using JSBeautifyLib;

namespace TheVoid.LiteServer
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            //new VoidWindow();

            // var ws = new WebServer(SendEvaluatedResponse, "http://+:6790/");
            // ws.Run();

            // while (true)
            // {
            //     Thread.Sleep(1000);
            //     // Console.Clear();
            //     // Console.WriteLine("z\n");
            // }

            TheVoid.Utility.Print("------- TheVoid -------");
            TheVoid.Utility.Print("Started:" + TheVoid.Combustion.Evaluate("default", "Date.now();", "system"));
            try
            {
                TheVoid.Utility.Print("------- Engines -------");
                foreach (var x in TheVoid.Combustion.ListEngines())
                    TheVoid.Utility.Print(x);
                Sockets.UDPServer(9067);
                TheVoid.Utility.Print("-------");
                TheVoid.Utility.Print("Open Host");
                TheVoid.Utility.Print("------- Midi -------");
                TheVoid.Utility.Print("Midi In Device:" + Midi.GetMIDIInDevices()[Midi.MidiInDeviceNumber]);
                TheVoid.Utility.Print("Midi Out Device:" + Midi.GetMIDIOutDevices()[Midi.MidiOutDeviceNumber]);
                ws.Run();
                ws2.Run();
            }
            catch (Exception ex)
            {
                TheVoid.Utility.Print(ex.Message);
            }
            TheVoid.Utility.Print("Init Complete");
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            ws.Stop();
            ws2.Stop();
        }


        static WebServer ws = new WebServer(SendResponse, "http://+:6789/");
        static WebServer ws2 = new WebServer(SendEvaluatedResponse, "http://+:6790/");
        public static string SendEvaluatedResponse(HttpListenerRequest request)
        {
            if (request.HttpMethod.ToUpper() == "POST")
                using (System.IO.Stream body = request.InputStream) // here we have data
                using (System.IO.StreamReader reader = new System.IO.StreamReader(body, request.ContentEncoding))
                    return TheVoid.Combustion.Evaluate("default", reader.ReadToEnd());
            return "ok=true";
        }
        public static string SendResponse(HttpListenerRequest request)
        {
            if (request.RawUrl.Contains("functions"))
            {
                string cmd = @"Object.keys(this).filter(function(x){ if (!(this[x] instanceof Function)) return false; return !/\[native code\]/.test(this[x].toString()) ? true : false;});/*do not log*/";
                string functionlist = Combustion.Evaluate("default", TheVoid.Combustion.Evaluate("default", cmd).Replace(',', '+'));
                return new JSBeautify(functionlist, new JSBeautifyOptions { preserve_newlines = true }).GetResult();
            }
            else if (request.RawUrl.Contains("variables"))
            {
                string thisjson = TheVoid.Combustion.Evaluate("default", "JSON.stringify(this);");
                return new JSBeautify(thisjson, new JSBeautifyOptions { preserve_newlines = true }).GetResult();
            }
            return TheVoid.WebServer.TheVoidIndexPage();
        }
    }
}
