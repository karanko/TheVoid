﻿using CoreWCF;

namespace TheVoid.Service
{
    [ServiceContract]
    public interface IService
    {

        [OperationContract]
        List<string> ListEngines();
        [OperationContract]
        void Execute(string engine, string command);
        [OperationContract]
        string Evaluate(string engine, string command);
        [OperationContract]
        void CreateEngines(string engine);

        [OperationContract]
        string[] ListMessages(int lastmessage);

    }
}
