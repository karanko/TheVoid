﻿namespace TheVoid.Service
{

    public class Service : IService
    {
        public List<string> ListEngines() => TheVoid.Combustion.ListEngines();
        public void Execute(string engine, string command) => TheVoid.Combustion.Execute(engine, command, "user");

        public string Evaluate(string engine, string command) => TheVoid.Combustion.Evaluate(engine, command, "user");

        public void CreateEngines(string engine) => TheVoid.Combustion.Execute(engine, "enginecreatedon = Date.now();", "system");

        public string[] ListMessages(int lastmessage = 0) => Utility.Messages.ToArray();
    }
}
