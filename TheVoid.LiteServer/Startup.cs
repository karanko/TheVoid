
using CoreWCF;
using CoreWCF.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace TheVoid
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServiceModelServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseServiceModel(builder =>
            {
                builder
                    .AddService<Service.Service>()
                    .AddServiceEndpoint<Service.Service, Service.IService>(new BasicHttpBinding(), "/TheVoid");
            });
        }
    }
}

